# Exec fish as the interactive shell (Avoids problems with /etc/profile)
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
KEYTIMEOUT=10
setopt autocd
unsetopt beep
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/kev/.zshrc'
autoload -Uz compinit
compinit
# End of lines added by compinstall

# User defined configuration

# Prompts
# PROMPT='%B%F{green}[%n@%m %~]$ %f%b'
PROMPT='%B%F{green}[%m %~]$ %f%b'
RPROMPT="${${KEYMAP/vicmd/$VIM_PROMPT}/(main|viins)/}"

# Enable the vim mode
bindkey -v
bindkey -M viins 'jk' vi-cmd-mode
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char

function zle-line-init zle-keymap-select {
    VIM_PROMPT="%B%F{green}[Normal] %f%b"
    RPROMPT="${${KEYMAP/vicmd/$VIM_PROMPT}/(main|viins)/}"
    zle reset-prompt
}

zle -N zle-line-init
zle -N zle-keymap-select

# Alias
# alias open='detach xdg-open'
alias ls='ls --color=auto --hide=__pycache__'
alias grep='grep --color=auto'
alias .='pwd'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias .......='cd ../../../../../..'
alias b='upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -P "state|to\ full|time to empty|percentage" | sed -e "s:^ \{1,\}\(.\{1,\}\):\1:"'
alias d='date'
alias cal='cal -m'
alias emacs='emacs --no-window-system'
alias eth='sudo light -1200'
alias home='sudo light -1800'
alias info='info --vi-keys'
alias words='nvim ~/Documents/Words'
alias octave='octave --no-gui'
# alias python='ipython'
alias t='ipython'
alias trans='trans -shell :es'
alias deepl='trans -shell :es'
alias calcurse='calcurse --read-only'
alias def='sdcv'
# alias cmus='TERM=xterm-termite cmus'
# alias slock='swaylock -u -c #000000'
alias shutdown='sudo shutdown now'
alias doc='dasht'
alias terms='nvim ~/Research/Terms'
alias pylab='ipython --pylab'

alias sync='upload'
alias sync-playground='playground-upload $(tesla-ip)'

alias dev='conda activate machine-learning'
alias radix='conda activate challenge-env'

alias doc-server='dasht-server | xargs -n1 w3m'

alias ipdb='ipdb3'

alias R='R --no-save'

alias rm='rm -I'

alias hdmi='MONITOR=HDMI-3 nohup polybar top &'

alias please='sudo !!'

alias pdf='zathura'

alias n='cmus-remote -C player-next'
alias p='cmus-remote -C player-prev'

alias run='python'

# For Jhbuild
export PATH=$PATH:~/.local/bin
export PATH=$PATH:~/.cargo/bin
#export TERM=xterm-color # Avoid all the issues related to xterm-terminfo

export GTK_PATH='/usr/lib/gtk-3.0' # Avoid gtk-canberra error
export VISUAL=nvim
export EDITOR=nvim
export NO_AT_BRIDGE=1 #Avoid DBUS accesibility Bug (Investigate).
export XDG_CONFIG_HOME=$HOME/.config
# Allow control-shift-t to open a new terminal in the same directory
export PYTHONBREAKPOINT=ipdb.set_trace


source /etc/profile.d/vte.sh

# Syntax highlight
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/kev/.opt/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/kev/.opt/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/kev/.opt/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/kev/.opt/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
