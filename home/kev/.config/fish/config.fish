function fish_right_prompt -d "Write out the right prompt"
        fish_default_mode_prompt
end

#alias open='detach xdg-open'
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias b='upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -P "state|to\ full|time to empty|percentage" | sed -e "s:^ \{1,\}\(.\{1,\}\):\1:"'
alias d='date'
alias cal='cal -m'
alias emacs='emacs --no-window-system'
alias eth='sudo light -1200'
#alias home='sudo light -1800'
alias night='sudo light -1700'
alias info='info --vi-keys'
alias words='nvim ~/Documents/Words'
alias octave='octave --no-gui'
#alias python='ipython' In order to avoid problems with conda
alias trans='trans -shell :es'
alias deepl='trans -shell :es'
alias matlab='matlab -nodesktop -nosplash'
alias calcurse='calcurse --read-only'
alias wlanAndroid='sudo dhcpcd enp0s20u1'


# Conda environment
source /home/kev/.opt/miniconda3/etc/fish/conf.d/conda.fish
