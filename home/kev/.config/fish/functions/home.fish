function home
        echo "Setting new screen bright..."
        set current (sudo light --current)
        set homeValue 153

        if math "$current<$homeValue" > /dev/null
                set aux (math "$homeValue - $current")
                sudo light +"$aux"
        else if math "$current>$homeValue" > /dev/null
                set aux (math "$current - $homeValue")
                sudo light -"$aux"
        else
                echo "Nothing to do"
        end
end
