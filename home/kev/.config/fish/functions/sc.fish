function sc -a regexPattern -d "Search for files using find command with regex"
        sudo find ./ -regex $regexPattern
end
