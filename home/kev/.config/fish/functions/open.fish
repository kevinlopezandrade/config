function open -a file
        echo "Opening $file with the default application"
        xdg-open "$file" > /dev/null 2>&1 &;disown
end
