function fish_prompt --description 'Write out the prompt'
    set_color -o $fish_color_cwd
	set -l home_escaped (echo -n $HOME | sed 's/\//\\\\\//g')
    set -l pwd (echo -n $PWD | sed "s/^$home_escaped/~/" | sed 's/ /%20/g')
    set -l prompt_symbol ''
    switch "$USER"
        case root toor
            set prompt_symbol '#'
        case '*'
            set prompt_symbol '$'
    end
    #printf "[%s@%s %s%s%s]%s " $USER (prompt_hostname) (set_color -o $fish_color_cwd) $pwd (set_color -o $fish_color_cwd) $prompt_symbol
    printf "[%s %s%s%s]%s " (prompt_hostname) (set_color -o $fish_color_cwd) $pwd (set_color -o $fish_color_cwd) $prompt_symbol
end
