" init.vim

set encoding=utf8            " Default encoding for buffers
set number                   " Show line numbers
set hidden                   " Change buffers without saving
set visualbell               " No more annoying sounds
set termguicolors            " Add true color support
set splitbelow               " Open splits below insted at bottom
set splitright               " Open splits vertical right instead of left
set updatetime=250           " For gitgutter
set expandtab                " Space intead of tabs
set ignorecase               " Ignore case in a search
set smartcase                " Override the 'ignorecase' if the search pattern contains upper case
set nohlsearch               " Disable the highlightning of all the matches in a search
set shortmess+=c             " Suppress the annoying 'match x of y', 'The only match' and 'Pattern not found' messages
set nostartofline            " Kept the cursor in the same column (if possible) when scrolling down
set cursorline               " Highlight the current line
set spelllang=en             " Set spelllang to english

"Filetype

filetype plugin indent on " Enables filetype detection and loads the specific identation and plugins to the detected filetype


"Code Formatting options

autocmd TermOpen * setlocal nonumber nocursorline | startinsert

"Workarounds
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
autocmd VimEnter,BufEnter,BufWinEnter * silent! iunmap <buffer> <M-">
autocmd VimEnter *
    \ let &statusline='%{bufferline#refresh_status()}'
      \ .bufferline#get_status_string()
      \ .'%='
      \ .'%y | '
      \ .'%p%% | '
      \ .'%c:%l:%L '
      \ .'%{LspInfo()}'


"Settings for some plugins

let g:filebeagle_suppress_keymaps = 1
let g:bufferline_echo = 0
let g:bufferline_active_buffer_left = ''
let g:bufferline_active_buffer_right = ''


"Shortcuts

let mapleader = "\<Space>"
nmap <leader>f  :redraw!<cr>
nnoremap <leader>h :LspHover<cr>
nnoremap <leader>z <C-W>z
nnoremap <leader>ev :e $MYVIMRC<cr>
nnoremap <leader>rv :source! $MYVIMRC<cr>
inoremap jk <Esc>
inoremap JK <Esc>
tnoremap <Esc> <C-\><C-n>
tnoremap jk <C-\><C-n>
cnoreabbrev W    w
cnoreabbrev Wq   wq
cnoreabbrev WQ   wq
cnoreabbrev Q    q
vnoremap <leader>c "+y
nnoremap <S-J><S-J> <C-W><C-W>
nnoremap <C-H> <C-W><C-H>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <silent> <leader>nn :FileBeagleBufferDir<cr>
nnoremap <leader>q :silent sus<cr>
nnoremap h <NOP>
nnoremap j <NOP>
nnoremap k <NOP>
nnoremap l <NOP>
inoremap <C-h> <C-O>:LspHover<cr>

"Plugins

call plug#begin('~/.local/share/nvim/plugged/')

Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'jeetsukumaran/vim-filebeagle'
Plug 'drewtempelmeyer/palenight.vim'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'prabirshrestha/async.vim'
Plug 'kevinlopezandrade/vim-lsp'
Plug 'prabirshrestha/asyncomplete-buffer.vim'
Plug 'prabirshrestha/asyncomplete-file.vim'
Plug 'bling/vim-bufferline'

call plug#end()

"Autocompletion Engine Settings


" call asyncomplete#register_source(asyncomplete#sources#buffer#get_source_options({
"     \ 'name': 'buffer',
"     \ 'whitelist': ['*'],
"     \ 'blacklist': ['go'],
"     \ 'completor': function('asyncomplete#sources#buffer#completor'),
"     \ }))

au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#file#get_source_options({
    \ 'name': 'file',
    \ 'whitelist': ['*'],
    \ 'priority': 10,
    \ 'completor': function('asyncomplete#sources#file#completor')
    \ }))

inoremap <expr> <CR> (pumvisible() ? "\<c-y>\<cr>" : "\<CR>")
set completeopt=noinsert,menuone,noselect

" au User lsp_setup call lsp#register_server({
"                         \ 'name': 'mspyls',
"                         \ 'cmd': {server_info->['mspyls']},
"                         \ 'whitelist': ['python'],
"                         \ 'initialization_options':
"                         \ {
"                         \       'interpreter':
"                         \       {
"                         \               'properties':
"                         \               {
"                         \                       'InterpreterPath': '/usr/bin/python',
"                         \                       'UseDefaultDatabase': 'true',
"                         \                       'Version': '3.7.2'
"                         \               }
"                         \       }
"                         \ },
"                         \ })

" au User lsp_setup call lsp#register_server({
"                         \ 'name': 'texlab',
"                         \ 'cmd': {server_info->['lsp-latex']},
"                         \ 'whitelist': ['ruby'],
"                         \ })

" let g:lsp_diagnostics_enabled = 0
" let g:lsp_sings_enabled = 0


"UI Settings

set background=dark
colorscheme palenight


"Custom Functions

function! LspInfo()
        let l:infoServer = lsp#get_server_status()
        if l:infoServer =~ "not"
                return ''
        else
                return l:infoServer
        endif
endfunction
