" init.vim

set encoding=utf8            " Default encoding for buffers
set number                   " Show line numbers
set hidden                   " Change buffers without saving
set visualbell               " No more annoying sounds
set termguicolors            " Add true color support
set splitbelow               " Open splits below insted at bottom
set splitright               " Open splits vertical right instead of left
set updatetime=250           " For gitgutter
set expandtab                " Space intead of tabs
set ignorecase               " Ignore case in a search
set smartcase                " Override the 'ignorecase' if the search pattern contains upper case
set nohlsearch               " Disable the highlightning of all the matches in a search
set shortmess+=c             " Suppress the annoying 'match x of y', 'The only match' and 'Pattern not found' messages
set shortmess+=s             " Suppres the messages of 'search hit BOTTOM'
set nostartofline            " Kept the cursor in the same column (if possible) when scrolling down
set cursorline               " Highlight the current line
set spelllang=en             " Set spelllang to english
" set relativenumber

"Filetype

" filetype plugin indent on " Enables filetype detection and loads the specific identation and plugins to the detected filetype

autocmd Filetype julia setlocal shiftwidth=4

"Code Formatting options

autocmd TermOpen * setlocal statusline=%{printf('')} nonumber nocursorline
autocmd ColorScheme * hi! link Sneak IncSearch

"Workarounds
" augroup customcmdline
"     au!
"     autocmd CmdlineLeave : echo ''
" augroup end

inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
" autocmd VimEnter,BufEnter,BufWinEnter * silent! iunmap <buffer> <M-">
autocmd VimEnter *
    \ let &statusline='%{bufferline#refresh_status()}'
      \ .bufferline#get_status_string()
      \ .'%='
      \ .'%y | '
      \ .'%p%% | '
      \ .'%c:%l:%L '


"Settings for some plugins

let g:filebeagle_suppress_keymaps = 1
let g:bufferline_echo = 0
let g:bufferline_active_buffer_left = ''
let g:bufferline_active_buffer_right = ''
let g:sneak#label = 1
let g:sneak#use_ic_scs = 1
let g:sneak#label_esc = ""
let g:goyo_width = '80%'
" let g:iron_map_defaults = 0
let g:python_highlight_space_errors = 0
let g:python_highlight_all = 1
let g:latex_to_unicode_auto = 1
let g:latex_to_unicode_file_types = ["julia", "python"]

"Shortcuts

let mapleader = "\<Space>"
let maplocalleader = "\<Space>"
nnoremap <Space> <NOP>
nnoremap <leader>f  :redraw!<cr>
nnoremap <leader>h :LspHover<cr>
nnoremap <leader>z <C-W>z
nnoremap <leader>ev :e $MYVIMRC<cr>
inoremap jk <Esc>
inoremap JK <Esc>
tnoremap <Esc> <C-\><C-n>
tnoremap jk <C-\><C-n>
cnoreabbrev W    w
cnoreabbrev Wq   wq
cnoreabbrev WQ   wq
cnoreabbrev Q    q
vnoremap <leader>c "+y
nnoremap <S-J><S-J> <C-W><C-W>
nnoremap <C-H> <C-W><C-H>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <silent> <leader>nn :FileBeagleBufferDir<cr>
nnoremap <leader>q :silent sus<cr>
" nnoremap h <NOP>
" nnoremap j <NOP>
" nnoremap k <NOP>
" nnoremap l <NOP>
inoremap <C-h> <C-O>:LspHover<cr>
nnoremap <leader>te :!xelatex %<cr>
nmap <leader>w <Plug>Sneak_s
nmap <leader><leader>w <Plug>Sneak_S
nnoremap <S-CR> <C-P>
nmap s <Nop>
xmap s <Nop>
" vmap <leader>r <Plug>(iron-send-motion)<ESC>
" cnoreabbrev reset IronSend! %reset -f
nnoremap <C-d> <S-l>zz
nnoremap <C-u> <S-h>zz

"Plugins

call plug#begin('~/.local/share/nvim/plugged/')

Plug 'tpope/vim-commentary'
Plug 'jeetsukumaran/vim-filebeagle'
Plug 'drewtempelmeyer/palenight.vim'
Plug 'ncm2/ncm2'
Plug 'roxma/nvim-yarp'
Plug 'ncm2/ncm2-bufword'
Plug 'ncm2/ncm2-path'
Plug 'prabirshrestha/async.vim'
Plug 'kevinlopezandrade/vim-lsp'
Plug 'ncm2/ncm2-vim-lsp'
Plug 'bling/vim-bufferline'
Plug 'lervag/vimtex'
Plug 'justinmk/vim-sneak'
Plug 'iCyMind/NeoSolarized'
Plug 'moll/vim-bbye'
Plug 'junegunn/goyo.vim'
Plug 'roxma/vim-window-resize-easy'
Plug 'machakann/vim-sandwich'
Plug 'markonm/traces.vim'
" Plug 'Vigemus/iron.nvim'
Plug 'morhetz/gruvbox'
Plug 'airblade/vim-gitgutter'
Plug 'godlygeek/tabular'
Plug 'vim-python/python-syntax'
Plug 'Vimjas/vim-python-pep8-indent'
Plug 'JuliaEditorSupport/julia-vim'
Plug 'jiangmiao/auto-pairs'

Plug 'ncm2/ncm2-jedi'

call plug#end()

"UI Settings
"
set background=dark
" colorscheme gruvbox
colorscheme palenight

"Autocompletion Engine Settings

set completeopt=noinsert,menuone,noselect
autocmd BufEnter * call ncm2#enable_for_buffer()

let s:julia_exe = '/usr/bin/julia'
let s:julia_lsp_startscript = '/home/kev/.local/bin/startlanguageserver.jl'

let g:lsp_diagnostics_enabled = 0
let g:lsp_sings_enabled = 0
let g:lsp_virtual_text_enabled = 0

""Python LSP
"au User lsp_setup call lsp#register_server({
"                        \ 'name': 'mspyls',
"                        \ 'cmd': {server_info->['mspyls']},
"                        \ 'whitelist': ['python'],
"                        \ 'initialization_options':
"                        \ {
"                        \       'interpreter':
"                        \       {
"                        \               'properties':
"                        \               {
"                        \                       'InterpreterPath': '/usr/bin/python',
"                        \                       'UseDefaultDatabase': 'true',
"                        \                       'Version': '3.7.3'
"                        \               }
"                        \       }
"                        \ },
"                        \ })

" Latex LSP
au User lsp_setup call lsp#register_server({
                        \ 'name': 'texlab',
                        \ 'cmd': {server_info->['lsp-latex']},
                        \ 'whitelist': ['tex', 'plaintex', 'latex'],
                        \ })

"Julia LSP
autocmd User lsp_setup call lsp#register_server({
                          \ 'name': 'julia',
                          \ 'cmd': {server_info->[s:julia_exe, '--startup-file=no', '--history-file=no', s:julia_lsp_startscript]},
                          \ 'whitelist': ['julia'],
                          \ })


" Latex Configutations
let g:tex_flavor = 'latex'
let g:vimtex_complete_close_braces = 1
let g:vimtex_compiler_method = 'latexmk'
" let g:vimtex_quickfix_mode = 0
let g:vimtex_quickfix_autoclose_after_keystrokes = 2
let g:vimtex_compiler_latexmk = {
                \ 'backend' : 'nvim', 
                \ 'background' : 1,
                \ 'build_dir' : '',
                \ 'callback' : 1,
                \ 'continuous' : 1,
                \ 'executable' : 'latexmk',
                \ 'options' : [
                        \   '-verbose',
                        \   '-file-line-error',
                        \   '-interaction=nonstopmode',
                \ ],
                \}

" autocmd Filetype tex call ncm2#register_source({
"     \ 'name': 'vimtex',
"     \ 'priority': 8,
"     \ 'scope': ['tex'],
"     \ 'mark': 'tex',
"     \ 'word_pattern': '\w+',
"     \ 'complete_pattern': g:vimtex#re#ncm2,
"     \ 'on_complete': ['ncm2#on_complete#omni', 'vimtex#complete#omnifunc'],
"     \ })

" luafile $HOME/.config/nvim/plugins.lua
